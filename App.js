import React from 'react';
import { Button, StyleSheet, Text, View } from 'react-native';


export default class app extends React.Component {
  state = {
    h: 0,
    m: 0,
    s: 0,
    ms: 0,

  }

  i = 0;

  timerStart = () => {
    this.i = setInterval(() => {

      this.setState(prevState => { return { ms: prevState.ms + 10 } });

      if (this.state.ms >= 99) {
        this.setState({ ms: 0 });
        this.setState(prevState => {
          return {
            s: prevState.s + 1
          }
        });

        if (this.state.s == 59) {
          this.setState({ s: 0 });
          this.setState(prevState => {
            return {
              m: prevState.m + 1
            }
          });

          if (this.state.m == 59) {
            this.setState({ m: 0 });
            this.setState(prevState => {
              return {
                h: prevState.h + 1
              }
            });

            if (this.state.h == 12) {
              this.setState({ h: 1 });
            }
          }
        }
      }

    }, 100);
  }

  timerStop = () => {
    clearInterval(this.i);
  }

  timerReset = () => {
    clearInterval(this.i);
    this.setState({
      h: 0,
      m: 0,
      s: 0,
      ms: 0
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{'marginVertical': 20}}>
          <Text style={{'fontSize': 22}}>Stop Watch</Text>
        </View>
        <View style={styles.buttons}>
            <Button title="stop"  onPress={this.timerStop} style={{'borderRadius': '15'}} color="navy" />
            <Button title="start" onPress={this.timerStart} color="teal" />
            <Button title="reset"  onPress={this.timerReset} color="black" />
        </View>
        <View style={styles.card}>
          <Text>{this.state.h} : {this.state.m} : {this.state.s} : {this.state.ms}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: 50,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: 22
  },
  buttons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '70%'
  },
  card: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: 'black',
    borderWidth: 1,
    borderRadius: 15,
    padding: 30,
    margin: 10,

  }
});
